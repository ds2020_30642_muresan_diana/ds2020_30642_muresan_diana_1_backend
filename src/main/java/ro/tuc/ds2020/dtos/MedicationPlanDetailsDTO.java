package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class MedicationPlanDetailsDTO {

    private UUID id;
    @NotNull
    private String intakeIntervals;
    @NotNull
    private String periodOfTreatment;
    @NotNull
    private Set<MedicationDetailsDTO> medications;



    public MedicationPlanDetailsDTO() {

    }

    public MedicationPlanDetailsDTO( @NotNull String intakeIntervals, @NotNull  String periodOfTreatment, @NotNull  Set<MedicationDetailsDTO> medications ) {
        this.intakeIntervals = intakeIntervals;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;

    }

    public MedicationPlanDetailsDTO(UUID id, @NotNull  String intakeIntervals, @NotNull  String periodOfTreatment, @NotNull  Set<MedicationDetailsDTO> medications ) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    public Set<MedicationDetailsDTO> getMedications() {
        return medications;
    }

    public void setMedications(Set<MedicationDetailsDTO> medications) {
        this.medications = medications;
    }


}
