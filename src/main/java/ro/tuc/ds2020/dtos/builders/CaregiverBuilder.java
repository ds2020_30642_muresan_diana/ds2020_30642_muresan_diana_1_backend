package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;

public class CaregiverBuilder {


    public CaregiverBuilder() {

    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getBirthdate().toString(), caregiver.getGender(), caregiver.getAddress(), caregiver.getUsername(), caregiver.getPassword());
    }

    public static Caregiver toCaregiver(CaregiverDetailsDTO caregiverDetailsDTO)  {
        try {
            return new Caregiver(caregiverDetailsDTO.getId(),caregiverDetailsDTO.getName(), new SimpleDateFormat("dd/MM/yyyy").parse(caregiverDetailsDTO.getBirthdate()), caregiverDetailsDTO.getGender(), caregiverDetailsDTO.getAddress(), caregiverDetailsDTO.getUsername(), caregiverDetailsDTO.getPassword());
        }catch (ParseException parseException) {
            System.out.println(parseException.getMessage());
            return null;
        }

    }

    public static CaregiverDetailsDTO toCaregiverDetailsDTO(Caregiver caregiver) {
        return new CaregiverDetailsDTO(caregiver.getId(), caregiver.getName(), caregiver.getBirthdate().toString(), caregiver.getGender(), caregiver.getAddress(), caregiver.getUsername(), caregiver.getPassword());
    }


}
