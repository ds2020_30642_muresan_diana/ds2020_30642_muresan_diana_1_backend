package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.entities.Account;

public class AccountBuilder {

    public AccountBuilder() {
    }

    public static AccountDTO toAccountDTO(Account user) {
        return new AccountDTO(user.getId(), user.getUsername(), user.getPassword(), user.getRole());

    }

    public static Account toAccount(AccountDetailsDTO userDetailsDTO)  {

            return new Account(userDetailsDTO.getId(), userDetailsDTO.getUsername(), userDetailsDTO.getPassword(), userDetailsDTO.getRole());

    }

    public static AccountDetailsDTO toAccountDetailsDTO(Account user) {
        return new AccountDetailsDTO(user.getId(), user.getUsername(), user.getPassword(), user.getRole());
    }
}
