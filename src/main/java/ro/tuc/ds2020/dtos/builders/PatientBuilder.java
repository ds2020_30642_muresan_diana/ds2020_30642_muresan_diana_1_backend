package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Patient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;


public class PatientBuilder {

    private static MedicationPlanBuilder medicationPlanBuilder = new MedicationPlanBuilder();
    public PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {

        return new PatientDTO(patient.getId(), patient.getName(), patient.getAddress(), patient.getBirthdate().toString(), patient.getGender(), patient.getMedicalRecord(), patient.getUsername(), patient.getPassword(), patient.getMedicationPlans().stream().map(medicationPlan -> medicationPlanBuilder.toMedicationPlanDTO(medicationPlan)).collect(Collectors.toSet()));

    }

    public static Patient toPatient(PatientDetailsDTO patientDetailsDTO)  {

        try {
            if (patientDetailsDTO.getMedicationPlans().isEmpty())
                return new Patient(patientDetailsDTO.getId(),patientDetailsDTO.getName(), patientDetailsDTO.getAddress(), new SimpleDateFormat("dd/MM/yyyy").parse(patientDetailsDTO.getBirthdate()), patientDetailsDTO.getGender(), patientDetailsDTO.getMedicalRecord(), patientDetailsDTO.getUsername(), patientDetailsDTO.getPassword(), null);
            else
                return new Patient(patientDetailsDTO.getId(),patientDetailsDTO.getName(), patientDetailsDTO.getAddress(), new SimpleDateFormat("dd/MM/yyyy").parse(patientDetailsDTO.getBirthdate()), patientDetailsDTO.getGender(), patientDetailsDTO.getMedicalRecord(), patientDetailsDTO.getUsername(), patientDetailsDTO.getPassword(), patientDetailsDTO.getMedicationPlans().stream().map(medicationPlanDTO -> medicationPlanBuilder.toMedicationPlan(medicationPlanDTO)).collect(Collectors.toSet()));
        }catch (ParseException parseException) {
            System.out.println(parseException.getMessage());
            return null;
        }
    }

    public static PatientDetailsDTO toPatientDetailsDTO(Patient patient) {
        return new PatientDetailsDTO(patient.getId(), patient.getName(), patient.getAddress(), patient.getBirthdate().toString(), patient.getGender(), patient.getMedicalRecord(), patient.getUsername(), patient.getPassword(), patient.getMedicationPlans().stream().map(medicationPlan -> medicationPlanBuilder.toMedicationPlanDetailsDTO(medicationPlan)).collect(Collectors.toSet()) );
    }


}
