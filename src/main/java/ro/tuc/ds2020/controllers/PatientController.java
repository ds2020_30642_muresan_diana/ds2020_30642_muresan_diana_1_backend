package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.AccountService;
import ro.tuc.ds2020.services.PatientService;

import java.sql.SQLOutput;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.hateoas.Link;

import javax.sound.midi.Soundbank;
import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
    private final PatientService patientService;


    @Autowired
    public PatientController(PatientService patientService ) {
        this.patientService = patientService;

    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/get-by-username/{username}")
    public ResponseEntity<PatientDetailsDTO> getPatientByUsername(@PathVariable("username") String patientUsername) {
        PatientDetailsDTO dto = patientService.getPatientByUsername(patientUsername);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value ="/find-by-caregiver/{username}")
    public ResponseEntity<List<PatientDTO>> getPatientsByCaregiver(@PathVariable("username") String username){
        List<PatientDTO> patients = patientService.findPatientsByCaregiver(username);
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @GetMapping(value ="/find-by-caregiver-id/{id}")
    public ResponseEntity<List<PatientDTO>> getPatientsByCaregiverId(@PathVariable("id") UUID id){
        List<PatientDTO> patients = patientService.findPatientsByCaregiverId(id);
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }


    @PostMapping(value = "/insert/{caregiver-name}")
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDetailsDTO patientDTO, @PathVariable("caregiver-name") String caregiverName) {
        UUID patientID = patientService.insert(patientDTO, caregiverName);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @PutMapping(value = "/assign-caregiver/{id}/{id-caregiver}")
    public ResponseEntity<UUID> assignCaregiver(@PathVariable("id") UUID patientId, @PathVariable("id-caregiver") UUID caregiverId) {
        UUID id = patientService.assignCaregiver(patientId, caregiverId);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDetailsDTO> getPatient(@PathVariable("id") UUID patientId) {
        PatientDetailsDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deletePatient(@PathVariable("id") UUID idPatient) {
        patientService.deletePatientById(idPatient);
        return new ResponseEntity<>(idPatient, HttpStatus.OK);
    }

    @PutMapping(value = {"/{caregiverName}"})
    public ResponseEntity<PatientDetailsDTO> updatePatient(@Valid @RequestBody PatientDetailsDTO patientDTO, @PathVariable("caregiverName") String caregiverName) {
        patientService.updatePatientById(patientDTO, caregiverName);
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

}
