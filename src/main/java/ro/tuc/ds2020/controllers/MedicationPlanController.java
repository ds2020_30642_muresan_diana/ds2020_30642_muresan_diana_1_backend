package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.MedicationService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication-plan")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;
    private final PatientService patientService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService, PatientService patientService) {
        this.medicationPlanService = medicationPlanService;
        this.patientService = patientService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlans();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @PostMapping(value = "/insert/{idPatient}")
    public ResponseEntity<UUID> insertMedicationPlan(@Valid @RequestBody MedicationPlanDetailsDTO medicationPlanDTO, @PathVariable("idPatient") UUID patientId) {
        UUID medicationPlanID = medicationPlanService.insert(medicationPlanDTO, patientId);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/get-by-id/{id}")
    public ResponseEntity<List<MedicationPlanDTO>> findMedicationPlansByPatient(@PathVariable("id") UUID patientId) {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlansByPatient(patientId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @PutMapping(value = "/update/{idMedicationPlan}/{idPatient}")
    public ResponseEntity<UUID> updateMedicationPlan(@PathVariable("idMedicationPlan") UUID idMedicationPlan, @PathVariable("idPatient") UUID idPatient, @Valid @RequestBody MedicationDetailsDTO medicationDTO) {
        UUID medicationPlanId = medicationPlanService.updateMedicationPlan(idMedicationPlan, idPatient, medicationDTO);
        return new ResponseEntity<>(idMedicationPlan,HttpStatus.OK);
    }



}
