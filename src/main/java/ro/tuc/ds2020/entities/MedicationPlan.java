package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="MedicationPlan")
public class MedicationPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "intakeIntervals", nullable = false)
    private String intakeIntervals;

    @Column(name = "periodOfTreatment", nullable = false)
    private String periodOfTreatment;

    @ManyToMany(cascade = {
            CascadeType.MERGE},
            fetch = FetchType.EAGER
    )
    @JoinTable(name = "medicationPlan_medication",
            joinColumns = @JoinColumn(name = "medicationPlan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id")
    )
    private Set<Medication> medications;


    public MedicationPlan() {

    }

    public MedicationPlan(UUID id, String intakeIntervals, String periodOfTreatment, Set<Medication> medications ) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;

    }

    public MedicationPlan(String intakeIntervals, String periodOfTreatment, Set<Medication> medications ) {
        this.intakeIntervals = intakeIntervals;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;


    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }


}
