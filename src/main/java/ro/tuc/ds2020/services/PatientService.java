package ro.tuc.ds2020.services;

import org.hibernate.type.PostgresUUIDType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.hibernate.jpa.TypedParameterValue;

@Service
public class PatientService  {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final AccountRepository accountRepository;
    private final CaregiverService caregiverService;



    @Autowired
    public PatientService(PatientRepository patientRepository, AccountRepository accountRepository, CaregiverService caregiverService) {
        this.patientRepository = patientRepository;
        this.accountRepository = accountRepository;
        this.caregiverService = caregiverService;
    }


    public List<PatientDTO> findPatientsByCaregiver(String username) {
        CaregiverDetailsDTO caregiverDTO = caregiverService.getCaregiverByUsername(username);
        System.out.println(caregiverDTO.getId());
        List<Optional<Patient>> patients = patientRepository.findPatientsByCaregiver(caregiverDTO.getId());
        List<Patient> p = new ArrayList<Patient>();
        System.out.println(patients.size());
        for (Optional<Patient> op : patients)
            p.add(op.get());
        return p.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }
    public List<PatientDTO>  findPatientsByCaregiverId(UUID id) {
        List<Optional<Patient>> patients = patientRepository.findPatientsByCaregiver(id);
        List<Patient> p = new ArrayList<Patient>();
        System.out.println(patients.size());
        for (Optional<Patient> op : patients)
            p.add(op.get());
        return p.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }


    public List<PatientDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(PatientDetailsDTO patientDTO, String caregiverName) {
        Patient patient = PatientBuilder.toPatient(patientDTO);
        patient = patientRepository.save(patient);
        PatientDetailsDTO p = getPatientByUsername(patient.getUsername());
        CaregiverDetailsDTO c = caregiverService.getCaregiverByName(caregiverName);
        assignCaregiver(p.getId(), c.getId());
        LOGGER.debug("Person with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public UUID assignCaregiver(UUID id, UUID idCaregiver) {
        Optional<Patient> oldPatientOptional = patientRepository.findById(id);
        Patient oldPatient = oldPatientOptional.get();
        Caregiver caregiver = new Caregiver();
        caregiver.setId(idCaregiver);
        oldPatient.setCaregiver(caregiver);
        Patient newPatient = patientRepository.save(oldPatient);
        return id;
    }

    public PatientDetailsDTO findPatientById(UUID id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDetailsDTO(patientOptional.get());
    }

    public void deletePatientById(UUID patientId) {
        Optional<Patient> patientOptional = patientRepository.findById(patientId);
        Patient patient = patientOptional.get();
        Optional<Account> accountOptional = accountRepository.getAccountByUsername(patient.getUsername());
        Account account = accountOptional.get();
        accountRepository.deleteById(account.getId());
        patientRepository.deleteById(patientId);

    }

    public PatientDetailsDTO updatePatientById(PatientDetailsDTO patientDTO, String caregiverName) {
        List<Account> accounts = accountRepository.findAll();
        Patient patient = PatientBuilder.toPatient(patientDTO);
        Optional<Patient> oldPatientOptional = patientRepository.findById(patient.getId());
        Patient oldPatient = oldPatientOptional.get();
        Account account = null;
        for (Account acc : accounts) {
            if (acc.getUsername().equals(oldPatient.getUsername())) {
                account = acc;
                break;
            }
        }
        account.setUsername(patient.getUsername());
        account.setPassword(patient.getPassword());
        accountRepository.save(account);
        oldPatient.setName(patient.getName());
        oldPatient.setAddress(patient.getAddress());
        oldPatient.setBirthdate(patient.getBirthdate());
        oldPatient.setGender(patient.getGender());
        oldPatient.setMedicalRecord(patient.getMedicalRecord());
        oldPatient.setUsername(patient.getUsername());
        oldPatient.setPassword(patient.getPassword());

        Patient newPatient = null;
        if (caregiverName.equals("notChanged")) {
             newPatient = patientRepository.save(oldPatient);
        }
        else {
            CaregiverDetailsDTO caregiverDetailsDTO = caregiverService.getCaregiverByName(caregiverName);

            Caregiver caregiver = new Caregiver();
            caregiver.setId(caregiverDetailsDTO.getId());
            oldPatient.setCaregiver(caregiver);
            newPatient = patientRepository.save(oldPatient);

        }
        return PatientBuilder.toPatientDetailsDTO(newPatient);

    }

    public PatientDetailsDTO getPatientByUsername(String username) {
        List<Patient> patientList = patientRepository.findAll();
        for (Patient p : patientList) {
            if (p.getUsername().equals(username))
                return PatientBuilder.toPatientDetailsDTO(p);
        }
        return  null;
    }




}
